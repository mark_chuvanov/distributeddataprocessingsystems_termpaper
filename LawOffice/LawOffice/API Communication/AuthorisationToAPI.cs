﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Web;

namespace LawOffice.API_Communication
{
	public static class AuthorisationToAPI
	{
		public static string LoginToAPI(string username,string password)
		{
			string url = "https://localhost:44334/api/authorisation/login";
			HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(url);
			httpRequest.Method = "POST";
			httpRequest.ContentType = "application/xml";
			string data = $"<Login><Username>{username}</Username><Password>{password}</Password></Login>";
			using (StreamWriter streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
			{
				streamWriter.Write(data);
			}
			string result = null;
			HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();
			using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
			{
				result = streamReader.ReadToEnd();
			}
			if (httpResponse.StatusCode == HttpStatusCode.OK)
			{
				JsonDocument doc = JsonDocument.Parse(result);
				JsonElement root = doc.RootElement;
				return root.GetProperty("token").ToString();
			}
			return null;
		}
	}
}