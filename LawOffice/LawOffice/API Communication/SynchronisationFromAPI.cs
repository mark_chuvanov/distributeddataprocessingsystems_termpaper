﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;
using LawOffice.Models;

namespace LawOffice.Synchronisation
{
	public class SynchronisationFromAPI
	{
		public void LoadCaseLegalProceduresFromAPI ()
		{
			LawOfficeDBContext db = new LawOfficeDBContext();
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://localhost:44334/api/CaseLegalProcedures");
			request.Accept = "application/xml";
			WebResponse response = request.GetResponse();
			XmlDocument doc = new XmlDocument();
			doc.Load(response.GetResponseStream());
			XmlElement root = doc.DocumentElement;
			foreach (XmlElement caseLegalProcedureNode in root)
			{
				int id = Convert.ToInt32(caseLegalProcedureNode["Id"].InnerText);
				CaseLegalProcedure caseLegalProcedure = db.CasesLegalProcedures.Find(id);
				if (caseLegalProcedure == null)
				{
					CaseLegalProcedure newCaseLegalProcedure = new CaseLegalProcedure()
					{
						CaseId = int.Parse(caseLegalProcedureNode["CaseId"].InnerText),
						LegalProcedureId = int.Parse(caseLegalProcedureNode["LegalProcedureId"].InnerText),
						Date = DateTime.Parse(caseLegalProcedureNode["Date"].InnerText),
						Result = caseLegalProcedureNode["Result"].InnerText
					};
					db.CasesLegalProcedures.Add(newCaseLegalProcedure);
				}
			}
			db.SaveChanges();
		}

		//public void LoadLawyerCasesFromAPI ()
		//{
		//	LawOfficeDBContext db = new LawOfficeDBContext();
		//	HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://localhost:44334/api/CaseLegalProcedures");
		//	request.Accept = "application/xml";
		//	WebResponse response = request.GetResponse();
		//	XmlDocument doc = new XmlDocument();
		//	doc.Load(response.GetResponseStream());
		//	XmlElement root = doc.DocumentElement;
		//	foreach (XmlElement caseLegalProcedureNode in root)
		//	{
		//		int id = Convert.ToInt32(caseLegalProcedureNode["Id"].InnerText);
		//		CaseLegalProcedure caseLegalProcedure = db.CasesLegalProcedures.Find(id);
		//		if (caseLegalProcedure == null)
		//		{
		//			CaseLegalProcedure newCaseLegalProcedure = new CaseLegalProcedure()
		//			{
		//				CaseId = int.Parse(caseLegalProcedureNode["CaseId"].InnerText),
		//				LegalProcedureId = int.Parse(caseLegalProcedureNode["LegalProcedureId"].InnerText),
		//				Date = DateTime.Parse(caseLegalProcedureNode["Date"].InnerText),
		//				Result = caseLegalProcedureNode["Result"].InnerText
		//			};
		//			db.CasesLegalProcedures.Add(newCaseLegalProcedure);
		//		}
		//	}
		//	db.SaveChanges();
		//}
	}
}