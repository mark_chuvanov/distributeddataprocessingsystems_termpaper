using LawOffice.Models.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace LawOffice
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            LawOfficeIdentityDBContext db = new LawOfficeIdentityDBContext();
            RoleStore<IdentityRole> roleStore = new RoleStore<IdentityRole>(db);
            RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(roleStore);
            if (!roleManager.RoleExists("Administrator"))
            {
                IdentityRole newRole = new IdentityRole("Administrator");
                roleManager.Create(newRole);
            }
            if (!roleManager.RoleExists("Client"))
            {
                IdentityRole newRole = new IdentityRole("Client");
                roleManager.Create(newRole);
            }
			if (!roleManager.RoleExists("Lawyer"))
			{
                IdentityRole newRole = new IdentityRole("Lawyer");
                roleManager.Create(newRole);
			}
        }
    }
}