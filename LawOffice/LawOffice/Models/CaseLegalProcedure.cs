﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LawOffice.Models
{
    public class CaseLegalProcedure
    {
        public int Id { get; set; }
        
        [Required]
        public int CaseId { get; set; }        
        public Case Case { get; set; }
        
        [Required]
        public int LegalProcedureId { get; set; }        
        public LegalProcedure LegalProcedure { get; set; }
        
        [Required, DisplayName("Дата оказания"), DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required, DisplayName("Результат оказания")]
        public string Result { get; set; }
    }
}