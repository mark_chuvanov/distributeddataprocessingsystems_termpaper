﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LawOffice.Models
{
    public class Lawyer
    {    
        public int Id { get; set; }
        
        [Required, DisplayName("Фамилия")]
        public string Surname { get; set; }
        
        [Required, DisplayName("Имя")]
        public string Name { get; set; }
        
        [DisplayName("Отчество (при наличии)")]
        public string Patronymic { get; set; }

        [NotMapped]
        public string FIO
        {
            get
            {
                return (Surname + " " + Name + " " + Patronymic);
            }
        }

        [Required, ScaffoldColumn(false), DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }
        
        [NotMapped, DisplayName("Стаж работы (лет)")]
        public string LengthOfService { 
            get
            {
                string length = ((DateTime.Now - StartDate).Days / 365).ToString();
                if (length == "0")
				{
                    return "менее года";
				}
				else
				{
                    return length;
				}
            } 
        }
        
        public byte[] Photo { get; set; }

        [Required, DisplayName("Адрес электронной почты"), DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        public int LawyerStatusId { get; set; }        
        public LawyerStatus LawyerStatus { get; set; }
        
        public ICollection<Case> Cases { get; set; }
        
        public Lawyer()
        {
            Cases = new List<Case>();
        }
    }
}