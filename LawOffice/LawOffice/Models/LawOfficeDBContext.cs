﻿using System.Data.Entity;

namespace LawOffice.Models
{
    public class LawOfficeDBContext : DbContext
    {
        public DbSet<Case> Cases { get; set; }
        public DbSet<CaseLegalProcedure> CasesLegalProcedures { get; set; }
        public DbSet<CaseResult> CaseResults { get; set; }
        public DbSet<CaseStatus> CaseStatuses { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Lawyer> Lawyers { get; set; }
        public DbSet<LawyerStatus> LawyerStatuses { get; set; }
        public DbSet<LegalProcedure> LegalProcedures { get; set; }
    }
}