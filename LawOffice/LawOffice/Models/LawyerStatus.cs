﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LawOffice.Models
{
    public class LawyerStatus
    {
        public int Id { get; set; }

        [Required, DisplayName("Статус")]
        public string Name { get; set; }

        public ICollection<Lawyer> Lawyers { get; set; }

        public LawyerStatus ()
        {
            Lawyers = new List<Lawyer>();
        }
    }
}