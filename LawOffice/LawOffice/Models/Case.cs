﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LawOffice.Models
{
    public class Case
    {
        public int Id { get; set; }
        
        [Required, DisplayName("Номер"), RegularExpression(@"(\d)*-[у|г|а|А]", ErrorMessage = "Указан неверный формат номера")]
        public string Number { get; set; }
        
        [Required]
        public int ClientId { get; set; }        
        public Client Client { get; set; }
        
        [Required, DisplayName("Дата открытия"), DataType(DataType.Date)]
        public DateTime OpeningDate { get; set; }
        
        [DisplayName("Дата закрытия"), DataType(DataType.Date)]
        public DateTime ClosingDate { get; set; }
        
        [Required]
        public int CaseStatusId { get; set; }
        public CaseStatus CaseStatus { get; set; }
        
        public int CaseResultId { get; set; }
        public CaseResult CaseResult { get; set; }
        
        [DisplayName("Стоимость (рублей)")]
        public int Costs { get; set; }
        
        public ICollection<Lawyer> Lawyers { get; set; }
        
        public Case()
        {
            Lawyers = new List<Lawyer>();
        }
    }
}