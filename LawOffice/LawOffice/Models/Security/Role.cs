﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LawOffice.Models.Security
{
	public class Role : IdentityRole
	{
		public Role () { }
		public Role (string name, string description) : base(name)
		{
			this.Description = description;
		}
		public string Description { get; set; }
	}
}