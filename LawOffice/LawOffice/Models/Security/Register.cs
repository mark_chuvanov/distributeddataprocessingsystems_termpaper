﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LawOffice.Models.Security
{
	public class Register
	{
		[Required, DisplayName("Имя пользователя")]
		public string UserName { get; set; }

		[DataType(DataType.Password), Required, DisplayName("Пароль")]
		public string Password { get; set; }

		[DataType(DataType.Password), Required, DisplayName("Подтверждение пароля")]
		[Compare("Password", ErrorMessage = "Пароли не совпадают")]
		public string ConfirmPassword { get; set; }

		[Required]
		[EmailAddress, DisplayName("Адрес электронной почты")]
		public string Email { get; set; }

		[Required]
		[DisplayName("Полное имя")]
		public string FullName { get; set; }

		[Required]
		[DisplayName("Дата рождения"), DataType(DataType.Date)]
		public DateTime BirthDate { get; set; }
	}
}