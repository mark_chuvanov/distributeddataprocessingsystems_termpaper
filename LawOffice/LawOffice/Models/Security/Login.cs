﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LawOffice.Models.Security
{
	public class Login
	{
		[Required, Display(Name = "Имя пользователя")]
		public string UserName { get; set; }

		[DataType(DataType.Password), Display(Name = "Пароль")]
		public string Password { get; set; }

		[Display(Name = "Запомнить меня?")]
		public bool IsRememberMe { get; set; }
	}
}