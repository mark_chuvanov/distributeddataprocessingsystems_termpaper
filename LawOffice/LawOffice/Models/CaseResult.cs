﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LawOffice.Models
{
    public class CaseResult
    {
        public int Id { get; set; }
        
        [Required, DisplayName("Результат")]
        public string Name { get; set; }
        
        public ICollection<Case> Cases { get; set; }
        
        public CaseResult()
        {
            Cases = new List<Case>();
        }
    }
}