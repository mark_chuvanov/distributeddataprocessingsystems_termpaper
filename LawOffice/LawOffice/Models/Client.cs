﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LawOffice.Models
{
    public class Client
    {
        public int Id { get; set; }

        [Required, DisplayName("Фамилия")]
        public string Surname { get; set; }

        [Required, DisplayName("Имя")]
        public string Name { get; set; }
        
        [DisplayName("Отчество (при наличии)")]
        public string Patronymic { get; set; }
        
        [Required, DisplayName("Дата рождения"), DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        
        [Required, Range(1000, 9999, ErrorMessage = "Серия паспорта должна состоять из четырех цифр"), DisplayName("Серия паспорта")]
        public int PassportSeries { get; set; }
        
        [Required, Range(100000, 999999, ErrorMessage = "Номер паспорта должен состоять из шести цифр"), DisplayName("Номер паспорта")]
        public int PassportNumber { get; set; }
        
        [Required, DisplayName("Место рождения")]
        public string PlaceOfResidence { get; set; }
        
        [DisplayName("Фотография")]
        public byte[] Photo { get; set; }

        [Required, DisplayName("Адрес электронной почты"), DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        
        public ICollection<Case> Cases { get; set; }
        
        public Client()
        {
            Cases = new List<Case>();
        }
    }
}