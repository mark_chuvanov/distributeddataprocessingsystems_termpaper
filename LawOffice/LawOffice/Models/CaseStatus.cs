﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LawOffice.Models
{
    public class CaseStatus
    {
        public int Id { get; set; }
        
        [Required, DisplayName("Статус")]
        public string Name { get; set; }
        
        public ICollection<Case> Cases { get; set; }
        
        public CaseStatus()
        {
            Cases = new List<Case>();
        }
    }
}