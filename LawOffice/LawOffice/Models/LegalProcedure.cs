﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LawOffice.Models
{
    public class LegalProcedure
    {
        public int Id { get; set; }
        
        [Required, DisplayName("Название")]
        public string Name { get; set; }
        
        [Required, DisplayName("Стоимость")]
        public int Costs { get; set; }
    }
}