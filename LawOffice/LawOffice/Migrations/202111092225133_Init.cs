namespace LawOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CaseResults",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Cases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(nullable: false),
                        ClientId = c.Int(nullable: false),
                        OpeningDate = c.DateTime(nullable: false),
                        ClosingDate = c.DateTime(nullable: false),
                        CaseStatusId = c.Int(nullable: false),
                        CaseResultId = c.Int(nullable: false),
                        Costs = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CaseResults", t => t.CaseResultId, cascadeDelete: true)
                .ForeignKey("dbo.CaseStatus", t => t.CaseStatusId, cascadeDelete: true)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId)
                .Index(t => t.CaseStatusId)
                .Index(t => t.CaseResultId);
            
            CreateTable(
                "dbo.CaseStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Surname = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Patronymic = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        PassportSeries = c.Int(nullable: false),
                        PassportNumber = c.Int(nullable: false),
                        PlaceOfResidence = c.String(nullable: false),
                        Photo = c.Binary(),
                        Email = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Lawyers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Surname = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Patronymic = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        Photo = c.Binary(),
                        Email = c.String(nullable: false),
                        LawyerStatusId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LawyerStatus", t => t.LawyerStatusId, cascadeDelete: true)
                .Index(t => t.LawyerStatusId);
            
            CreateTable(
                "dbo.LawyerStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CaseLegalProcedures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CaseId = c.Int(nullable: false),
                        LegalProcedureId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Result = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.CaseId, cascadeDelete: true)
                .ForeignKey("dbo.LegalProcedures", t => t.LegalProcedureId, cascadeDelete: true)
                .Index(t => t.CaseId)
                .Index(t => t.LegalProcedureId);
            
            CreateTable(
                "dbo.LegalProcedures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Costs = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LawyerCases",
                c => new
                    {
                        Lawyer_Id = c.Int(nullable: false),
                        Case_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Lawyer_Id, t.Case_Id })
                .ForeignKey("dbo.Lawyers", t => t.Lawyer_Id, cascadeDelete: true)
                .ForeignKey("dbo.Cases", t => t.Case_Id, cascadeDelete: true)
                .Index(t => t.Lawyer_Id)
                .Index(t => t.Case_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CaseLegalProcedures", "LegalProcedureId", "dbo.LegalProcedures");
            DropForeignKey("dbo.CaseLegalProcedures", "CaseId", "dbo.Cases");
            DropForeignKey("dbo.Lawyers", "LawyerStatusId", "dbo.LawyerStatus");
            DropForeignKey("dbo.LawyerCases", "Case_Id", "dbo.Cases");
            DropForeignKey("dbo.LawyerCases", "Lawyer_Id", "dbo.Lawyers");
            DropForeignKey("dbo.Cases", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.Cases", "CaseStatusId", "dbo.CaseStatus");
            DropForeignKey("dbo.Cases", "CaseResultId", "dbo.CaseResults");
            DropIndex("dbo.LawyerCases", new[] { "Case_Id" });
            DropIndex("dbo.LawyerCases", new[] { "Lawyer_Id" });
            DropIndex("dbo.CaseLegalProcedures", new[] { "LegalProcedureId" });
            DropIndex("dbo.CaseLegalProcedures", new[] { "CaseId" });
            DropIndex("dbo.Lawyers", new[] { "LawyerStatusId" });
            DropIndex("dbo.Cases", new[] { "CaseResultId" });
            DropIndex("dbo.Cases", new[] { "CaseStatusId" });
            DropIndex("dbo.Cases", new[] { "ClientId" });
            DropTable("dbo.LawyerCases");
            DropTable("dbo.LegalProcedures");
            DropTable("dbo.CaseLegalProcedures");
            DropTable("dbo.LawyerStatus");
            DropTable("dbo.Lawyers");
            DropTable("dbo.Clients");
            DropTable("dbo.CaseStatus");
            DropTable("dbo.Cases");
            DropTable("dbo.CaseResults");
        }
    }
}
