﻿using LawOffice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LawOffice.ViewModels
{
	public class ClientDetails
	{
		public Client Client { get; set; }
		public IEnumerable<Case> Cases { get; set; }
	}
}