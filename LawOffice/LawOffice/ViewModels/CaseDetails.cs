﻿using LawOffice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LawOffice.ViewModels
{
	public class CaseDetails
	{
		public Case Case { get; set; }
		public IEnumerable<Lawyer> Lawyers { get; set; }
		public IEnumerable<CaseLegalProcedure> CaseLegalProcedures { get; set; }
	}
}