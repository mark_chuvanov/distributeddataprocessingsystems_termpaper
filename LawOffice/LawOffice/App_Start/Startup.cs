﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Threading.Tasks;

[assembly: OwinStartup(typeof(LawOffice.App_Start.Startup))]

namespace LawOffice.App_Start
{
	public class Startup
	{
		public void Configuration (IAppBuilder app)
		{
			CookieAuthenticationOptions options = new CookieAuthenticationOptions();
			options.AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie;
			options.LoginPath = new PathString("/account/login");
			app.UseCookieAuthentication(options);
		}
	}
}
