﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using LawOffice.Models;
using OfficeOpenXml.FormulaParsing.LexicalAnalysis;

namespace LawOffice.Controllers
{
	[Authorize]
	public class LegalProceduresController : Controller
	{
		private LawOfficeDBContext db = new LawOfficeDBContext();

		// GET: LegalProcedures
		public ActionResult Index ()
		{
			if (User.IsInRole("Administrator") || User.IsInRole("Client"))
			{
				return View(db.LegalProcedures.ToList());
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		public ActionResult LoadFromAPI ()
		{
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://localhost:44334/api/LegalProcedures");
			request.Accept = "application/xml";
			WebResponse response = request.GetResponse();
			XmlDocument doc = new XmlDocument();
			doc.Load(response.GetResponseStream());
			XmlElement root = doc.DocumentElement;
			foreach (XmlElement legalProcedureNode in root)
			{
				int id = Convert.ToInt32(legalProcedureNode["Id"].InnerText);
				LegalProcedure legalProcedure = db.LegalProcedures.Find(id);
				if (legalProcedure == null)
				{
					LegalProcedure newlegalProcedure = new LegalProcedure()
					{
						Name = legalProcedureNode["Name"].InnerText,
						Costs = int.Parse(legalProcedureNode["Costs"].InnerText),
					};
					db.LegalProcedures.Add(newlegalProcedure);
				}
				else
				{
					legalProcedure.Name = legalProcedureNode["Name"].InnerText;
					legalProcedure.Costs = int.Parse(legalProcedureNode["Costs"].InnerText);
				}
			}
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		// GET: LegalProcedures/Details/5
		//public ActionResult Details (int? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	LegalProcedure legalProcedure = db.LegalProcedures.Find(id);
		//	if (legalProcedure == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(legalProcedure);
		//}

		// GET: LegalProcedures/Create
		public ActionResult Create ()
		{
			if (User.IsInRole("Administrator"))
			{
				return View();
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// POST: LegalProcedures/Create
		// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
		// сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create ([Bind(Include = "Id,Name,Costs")] LegalProcedure legalProcedure)
		{
			if (User.IsInRole("Administrator"))
			{
				if (legalProcedure.Costs <= 0)
				{
					ModelState.AddModelError("Costs", "Стоимость процедуры указана некорректно");
				}
				if (ModelState.IsValid)
				{
					db.LegalProcedures.Add(legalProcedure);
					db.SaveChanges();
					HttpStatusCode code = SendToAPI(legalProcedure, "POST");
					switch (code)
					{
						case HttpStatusCode.Created:
						{
							return RedirectToAction("Index");
						}
						default:
						{
							return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
						}
					}
				}

				return View(legalProcedure);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// GET: LegalProcedures/Edit/5
		public ActionResult Edit (int? id)
		{
			if (User.IsInRole("Administrator"))
			{
				if (id == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				LegalProcedure legalProcedure = db.LegalProcedures.Find(id);
				if (legalProcedure == null)
				{
					return HttpNotFound();
				}
				return View(legalProcedure);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// POST: LegalProcedures/Edit/5
		// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
		// сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit ([Bind(Include = "Id,Name,Costs")] LegalProcedure legalProcedure)
		{
			if (User.IsInRole("Administrator"))
			{
				if (legalProcedure.Costs <= 0)
				{
					ModelState.AddModelError("Costs", "Стоимость процедуры указана некорректно");
				}
				if (ModelState.IsValid)
				{
					db.Entry(legalProcedure).State = EntityState.Modified;
					db.SaveChanges();
					HttpStatusCode code = SendToAPI(legalProcedure, "PUT");
					switch (code)
					{
						case HttpStatusCode.NoContent:
						{
							return RedirectToAction("Index");
						}
						default:
						{
							return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
						}
					}
				}
				return View(legalProcedure);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		private XmlElement PackLegalProcedureToXml (XmlDocument doc, LegalProcedure legalProcedure)
		{
			XmlElement legalProcedureNode = doc.CreateElement("LegalProcedure");
			XmlElement IdNode = doc.CreateElement("Id");
			IdNode.InnerText = legalProcedure.Id.ToString();
			legalProcedureNode.AppendChild(IdNode);

			XmlElement NameNode = doc.CreateElement("Name");
			NameNode.InnerText = legalProcedure.Name;
			legalProcedureNode.AppendChild(NameNode);

			XmlElement CostsNode = doc.CreateElement("Costs");
			CostsNode.InnerText = legalProcedure.Costs.ToString();
			legalProcedureNode.AppendChild(CostsNode);

			return legalProcedureNode;
		}

		private HttpStatusCode SendToAPI (LegalProcedure legalProcedure, string metka)
		{
			string token = API_Communication.AuthorisationToAPI.LoginToAPI("administrator", "Administrator_1");
			if (token == null)
			{
				return HttpStatusCode.Unauthorized;
			}
			XmlDocument doc = new XmlDocument();
			doc.CreateXmlDeclaration("1.0", "utf-8", "no");
			XmlElement legalProcedureNode = PackLegalProcedureToXml(doc, legalProcedure);
			doc.AppendChild(legalProcedureNode);
			string xml = doc.OuterXml;
			byte[] byteArray = Encoding.UTF8.GetBytes(xml);
			HttpWebRequest request;
			if (metka == "PUT")
			{
				request = (HttpWebRequest)WebRequest.Create($"https://localhost:44334/api/LegalProcedures/{legalProcedure.Id}");
			}
			else
			{
				request = (HttpWebRequest)WebRequest.Create("https://localhost:44334/api/LegalProcedures");
			}
			request.Headers.Add("Authorization", "Bearer " + token);
			request.Method = metka;
			request.ContentType = "application/xml; encoding='utf-8'";
			request.ContentLength = byteArray.Length;
			var reqStream = request.GetRequestStream();
			reqStream.Write(byteArray, 0, byteArray.Length);
			reqStream.Close();
			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			return response.StatusCode;
		}

		// GET: LegalProcedures/Delete/5
		//public ActionResult Delete (int? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	LegalProcedure legalProcedure = db.LegalProcedures.Find(id);
		//	if (legalProcedure == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(legalProcedure);
		//}

		// POST: LegalProcedures/Delete/5
		//[HttpPost, ActionName("Delete")]
		//[ValidateAntiForgeryToken]
		//public ActionResult DeleteConfirmed (int id)
		//{
		//	LegalProcedure legalProcedure = db.LegalProcedures.Find(id);
		//	db.LegalProcedures.Remove(legalProcedure);
		//	db.SaveChanges();
		//	return RedirectToAction("Index");
		//}

		protected override void Dispose (bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
