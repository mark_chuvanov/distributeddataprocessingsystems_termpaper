﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LawOffice.Controllers
{
	public class HomeController : Controller
	{
		[Authorize]
		public ActionResult Index ()
		{
			return View();
		}

		[Authorize]

		public ActionResult Report ()
		{
			if (User.IsInRole("Administrator"))
			{
				List<ReportType> types = new List<ReportType>()
			{
				new ReportType { typeId = 1, Type = "Доходы" },
				new ReportType { typeId = 2, Type = "Дела за выбранный период" },
				new ReportType { typeId = 3, Type = "Деятельность адвокатов" }
			};
				ViewBag.types = new SelectList(types, "typeId", "Type");
				List<ReportFormat> formats = new List<ReportFormat>()
			{
				new ReportFormat { formatId = 1, Format = "Excel" },
				new ReportFormat { formatId = 2, Format = "PDF" }
			};
				ViewBag.formats = new SelectList(formats, "formatId", "Format");
				return View();
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		[HttpPost]
		[Authorize]
		public ActionResult Report (int typeId, int formatId, DateTime? startDate, DateTime? endDate)
		{
			if (User.IsInRole("Administrator"))
			{
				switch (typeId)
				{
					case 1:
						{
							return RedirectToAction("GetEarnings", "Cases", new { id = formatId });
						}
					case 2:
						{
							return RedirectToAction("GetCases", "Cases", new { id = formatId, startDate = startDate, endDate = endDate });
						}
					case 3:
						{
							return RedirectToAction("GetLawyersStatistics", "Lawyers", new { id = formatId });
						}
				}
				List<ReportType> types = new List<ReportType>()
				{
				new ReportType { typeId = 1, Type = "Доходы" },
				new ReportType { typeId = 2, Type = "Клиенты" },
				new ReportType { typeId = 3, Type = "Деятельность адвокатов" }
				};
				ViewBag.types = new SelectList(types, "typeId", "Type");
				List<ReportFormat> formats = new List<ReportFormat>()
				{
				new ReportFormat { formatId = 1, Format = "Excel" },
				new ReportFormat { formatId = 2, Format = "PDF" }
				};
				ViewBag.formats = new SelectList(formats, "formatId", "Format");
				return View();
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}
	}
	class ReportType
	{
		public int typeId { get; set; }
		public string Type { get; set; }
	}
	class ReportFormat
	{
		public int formatId { get; set; }
		public string Format { get; set; }
	}
}