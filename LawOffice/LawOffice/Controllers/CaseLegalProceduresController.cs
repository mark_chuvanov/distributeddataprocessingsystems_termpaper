﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using LawOffice.Models;

namespace LawOffice.Controllers
{
	[Authorize]
	public class CaseLegalProceduresController : Controller
	{
		private LawOfficeDBContext db = new LawOfficeDBContext();

		// GET: CaseLegalProcedures
		public ActionResult Index ()
		{
			if (User.IsInRole("Administrator") || User.IsInRole("Client"))
			{
				var casesLegalProcedure = db.CasesLegalProcedures.Include(c => c.Case).Include(c => c.LegalProcedure);
				return View(casesLegalProcedure.ToList());
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// GET: CaseLegalProcedures/Details/5
		//public ActionResult Details (int? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	CaseLegalProcedure caseLegalProcedure = db.CasesLegalProcedure.Find(id);
		//	if (caseLegalProcedure == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(caseLegalProcedure);
		//}

		// GET: CaseLegalProcedures/Create
		public ActionResult Create (int id)
		{
			if (User.IsInRole("Administrator") || User.IsInRole("Lawyer"))
			{
				ViewBag.CaseId = new SelectList(db.Cases, "Id", "Number");
				ViewBag.LegalProcedureId = new SelectList(db.LegalProcedures, "Id", "Name");
				ViewData["CaseId"] = id;
				return View();
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// POST: CaseLegalProcedures/Create
		// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
		// сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create ([Bind(Include = "Id,CaseId,LegalProcedureId,Date,Result")] CaseLegalProcedure caseLegalProcedure)
		{
			if (User.IsInRole("Administrator") || User.IsInRole("Lawyer"))
			{
				if (ModelState.IsValid)
				{
					db.CasesLegalProcedures.Add(caseLegalProcedure);
					db.SaveChanges();
					return RedirectToAction("Details", "Cases", new { id = caseLegalProcedure.CaseId });
				}

				ViewBag.CaseId = new SelectList(db.Cases, "Id", "Number", caseLegalProcedure.CaseId);
				ViewBag.LegalProcedureId = new SelectList(db.LegalProcedures, "Id", "Name", caseLegalProcedure.LegalProcedureId);
				return View(caseLegalProcedure);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// GET: CaseLegalProcedures/Edit/5
		//public ActionResult Edit (int? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	CaseLegalProcedure caseLegalProcedure = db.CasesLegalProcedure.Find(id);
		//	if (caseLegalProcedure == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	ViewBag.CaseId = new SelectList(db.Cases, "Id", "Number", caseLegalProcedure.CaseId);
		//	ViewBag.LegalProcedureId = new SelectList(db.LegalProcedures, "Id", "Name", caseLegalProcedure.LegalProcedureId);
		//	return View(caseLegalProcedure);
		//}

		// POST: CaseLegalProcedures/Edit/5
		// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
		// сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public ActionResult Edit ([Bind(Include = "Id,CaseId,LegalProcedureId,Date,Result")] CaseLegalProcedure caseLegalProcedure)
		//{
		//	if (ModelState.IsValid)
		//	{
		//		db.Entry(caseLegalProcedure).State = EntityState.Modified;
		//		db.SaveChanges();
		//		return RedirectToAction("Index");
		//	}
		//	ViewBag.CaseId = new SelectList(db.Cases, "Id", "Number", caseLegalProcedure.CaseId);
		//	ViewBag.LegalProcedureId = new SelectList(db.LegalProcedures, "Id", "Name", caseLegalProcedure.LegalProcedureId);
		//	return View(caseLegalProcedure);
		//}

		// GET: CaseLegalProcedures/Delete/5
		//public ActionResult Delete (int? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	CaseLegalProcedure caseLegalProcedure = db.CasesLegalProcedure.Find(id);
		//	if (caseLegalProcedure == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(caseLegalProcedure);
		//}

		// POST: CaseLegalProcedures/Delete/5
		//[HttpPost, ActionName("Delete")]
		//[ValidateAntiForgeryToken]
		//public ActionResult DeleteConfirmed (int id)
		//{
		//	CaseLegalProcedure caseLegalProcedure = db.CasesLegalProcedure.Find(id);
		//	db.CasesLegalProcedure.Remove(caseLegalProcedure);
		//	db.SaveChanges();
		//	return RedirectToAction("Index");
		//}

		protected override void Dispose (bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
