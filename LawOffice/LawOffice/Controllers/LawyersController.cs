﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using LawOffice.Models;
using LawOffice.Models.Security;
using LawOffice.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using OfficeOpenXml;

namespace LawOffice.Controllers
{
	[Authorize]
	public class LawyersController : Controller
	{
		private LawOfficeDBContext db = new LawOfficeDBContext();

		// GET: Lawyers
		public ActionResult Index ()
		{
			if (User.IsInRole("Administrator"))
			{
				var lawyers = db.Lawyers.Include(l => l.LawyerStatus);
				return View(lawyers.ToList());
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		public ActionResult LoadFromAPI ()
		{
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://localhost:44334/api/Lawyers");
			request.Accept = "application/xml";
			WebResponse response = request.GetResponse();
			XmlDocument doc = new XmlDocument();
			doc.Load(response.GetResponseStream());
			XmlElement root = doc.DocumentElement;
			foreach (XmlElement lawyerNode in root)
			{
				int id = Convert.ToInt32(lawyerNode["Id"].InnerText);
				Lawyer lawyer = db.Lawyers.Find(id);
				if (lawyer == null)
				{
					Lawyer newLawyer = new Lawyer()
					{
						Surname = lawyerNode["Surname"].InnerText,
						Name = lawyerNode["Name"].InnerText,
						Patronymic = lawyerNode["Patronymic"].InnerText,
						StartDate = DateTime.Parse(lawyerNode["StartDate"].InnerText),
						LawyerStatusId = int.Parse(lawyerNode["LawyerStatusId"].InnerText),
						Email = lawyerNode["Email"].InnerText,
					};
					db.Lawyers.Add(newLawyer);
				}
				else
				{
					lawyer.Surname = lawyerNode["Surname"].InnerText;
					lawyer.Name = lawyerNode["Name"].InnerText;
					lawyer.Patronymic = lawyerNode["Patronymic"].InnerText;
					lawyer.StartDate = DateTime.Parse(lawyerNode["StartDate"].InnerText);
					lawyer.LawyerStatusId = int.Parse(lawyerNode["LawyerStatusId"].InnerText);
				}
			}
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		// GET: Lawyers/Details/5
		public ActionResult Details (int? id)
		{
			if (User.IsInRole("Lawyer"))
			{
				LawOfficeIdentityDBContext dbIdentity = new LawOfficeIdentityDBContext();
				UserStore<User> userStore = new UserStore<User>(dbIdentity);
				UserManager<User> userManager = new UserManager<User>(userStore);
				RoleStore<Role> roleStore = new RoleStore<Role>(dbIdentity);
				RoleManager<Role> roleManager = new RoleManager<Role>(roleStore);
				User user = userManager.FindByName(HttpContext.User.Identity.Name);
				try
				{
					id = db.Lawyers.Where(l => l.Email == user.Email).FirstOrDefault().Id;
				}
				catch { }
				if (id == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				LawyerDetails lawyerDetails = new LawyerDetails();
				lawyerDetails.Lawyer = db.Lawyers.Include(l => l.Cases).Where(l => l.Id == id).FirstOrDefault();
				lawyerDetails.Cases = db.Cases.Include(c => c.CaseResult).Include(c => c.CaseStatus).Where(c => c.Lawyers.Contains(db.Lawyers.Where(l => l.Id == id).FirstOrDefault())).ToList();
				if (lawyerDetails.Lawyer == null)
				{
					return HttpNotFound();
				}
				return View(lawyerDetails);
			}
			else
			{
				if (User.IsInRole("Administrator"))
				{
					if (id == null)
					{
						return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
					}
					LawyerDetails lawyerDetails = new LawyerDetails();
					lawyerDetails.Lawyer = db.Lawyers.Include(l => l.Cases).Where(l => l.Id == id).FirstOrDefault();
					lawyerDetails.Cases = db.Cases.Include(c => c.CaseResult).Include(c => c.CaseStatus).Where(c => c.Lawyers.Contains(db.Lawyers.Where(l => l.Id == id).FirstOrDefault())).ToList();
					if (lawyerDetails.Lawyer == null)
					{
						return HttpNotFound();
					}
					return View(lawyerDetails);
				}
				else
				{
					return new HttpStatusCodeResult(403);
				}
			}
		}

		// GET: Lawyers/Create
		public ActionResult Create ()
		{
			if (User.IsInRole("Administrator"))
			{
				ViewBag.LawyerStatusId = new SelectList(db.LawyerStatuses, "Id", "Name");
				Lawyer lawyer = new Lawyer();
				lawyer.StartDate = DateTime.Now;
				return View(lawyer);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// POST: Lawyers/Create
		// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
		// сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create ([Bind(Include = "Id,Surname,Name,Patronymic,StartDate,Photo,LawyerStatusId")] Lawyer lawyer)
		{
			if (User.IsInRole("Administrator"))
			{
				lawyer.StartDate = DateTime.Now;
				lawyer.LawyerStatusId = 1;
				if (ModelState.IsValid)
				{
					db.Lawyers.Add(lawyer);
					db.SaveChanges();
					return RedirectToAction("Index");
				}

				ViewBag.LawyerStatusId = new SelectList(db.LawyerStatuses, "Id", "Name", lawyer.LawyerStatusId);
				return View(lawyer);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// GET: Lawyers/Edit/5
		public ActionResult Edit (int? id)
		{
			if (User.IsInRole("Administrator"))
			{
				if (id == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				Lawyer lawyer = db.Lawyers.Find(id);
				if (lawyer == null)
				{
					return HttpNotFound();
				}
				ViewBag.LawyerStatusId = new SelectList(db.LawyerStatuses, "Id", "Name", lawyer.LawyerStatusId);
				return View(lawyer);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// POST: Lawyers/Edit/5
		// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
		// сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit ([Bind(Include = "Id,Surname,Name,Patronymic,StartDate,Photo,LawyerStatusId")] Lawyer lawyer)
		{
			if (User.IsInRole("Administrator"))
			{
				if (ModelState.IsValid)
				{
					db.Entry(lawyer).State = EntityState.Modified;
					db.SaveChanges();
					return RedirectToAction("Index");
				}
				ViewBag.LawyerStatusId = new SelectList(db.LawyerStatuses, "Id", "Name", lawyer.LawyerStatusId);
				return View(lawyer);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		public ActionResult Add (int id)
		{
			if (User.IsInRole("Administrator"))
			{
				var lawyers = db.Lawyers.Include(l => l.LawyerStatus).Include(l => l.Cases).ToList();
				for (int i = 0; i < lawyers.Count; i++)
				{
					foreach (var @case in lawyers[i].Cases)
					{
						if (@case.Id == id)
						{
							lawyers.RemoveAt(i);
						}
					}
				}
				ViewData["CaseId"] = id;
				ViewBag.LawyerId = new SelectList(lawyers, "Id", "FIO");
				return View();
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Add (int LawyerId, int CaseId)
		{
			if (User.IsInRole("Administrator"))
			{
				if (ModelState.IsValid)
				{
					var lawyer = db.Lawyers.Include(l => l.Cases).Where(l => l.Id == LawyerId).First();
					lawyer.Cases.Add(db.Cases.Where(c => c.Id == CaseId).First());
					db.SaveChanges();
					return RedirectToAction("Details", "Cases", new { id = CaseId });
				}
				var lawyers = db.Lawyers.Include(l => l.LawyerStatus).Include(l => l.Cases).ToList();
				for (int i = 0; i < lawyers.Count; i++)
				{
					foreach (var @case in lawyers[i].Cases)
					{
						if (@case.Id == (int)ViewData["CaseId"])
						{
							lawyers.RemoveAt(i);
						}
					}
				}
				ViewBag.LawyerId = new SelectList(lawyers, "Id", "Surname");
				return View();
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// GET: Lawyers/Delete/5
		//public ActionResult Delete (int? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	Lawyer lawyer = db.Lawyers.Find(id);
		//	if (lawyer == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(lawyer);
		//}

		// POST: Lawyers/Delete/5
		//[HttpPost, ActionName("Delete")]
		//[ValidateAntiForgeryToken]
		//public ActionResult DeleteConfirmed (int id)
		//{
		//	Lawyer lawyer = db.Lawyers.Find(id);
		//	db.Lawyers.Remove(lawyer);
		//	db.SaveChanges();
		//	return RedirectToAction("Index");
		//}

		[Authorize(Roles = "Administrator")]
		public FileResult GetLawyersStatistics (int id)
		{
			if (id == 1)
			{
				ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
				string originalFilePath = Server.MapPath("~/Content/Reports/lawyer_statistics.xlsx");
				FileInfo originalFileInfo = new FileInfo(originalFilePath);
				string resultFilePath = Server.MapPath("~/Content/Reports/report.xlsx");
				FileInfo resultFileInfo = new FileInfo(resultFilePath);
				using (ExcelPackage excelPackage = new ExcelPackage(originalFileInfo))
				{
					excelPackage.Workbook.Properties.Author = "Mark Chuvanov";
					excelPackage.Workbook.Properties.Title = "Статистика по адвокатам";
					excelPackage.Workbook.Properties.Subject = "Результаты деятельности адвокатов";
					excelPackage.Workbook.Properties.Created = DateTime.Now;
					ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets["Statistics"];
					int startLine = 3;
					var lawyers = db.Lawyers.Include(l => l.Cases).ToList();
					foreach (var lawyer in lawyers)
					{
						worksheet.Cells[startLine, 1].Value = lawyer.Surname;
						worksheet.Cells[startLine, 2].Value = lawyer.Name;
						worksheet.Cells[startLine, 3].Value = lawyer.Patronymic;
						worksheet.Cells[startLine, 4].Value = lawyer.Cases.Count;
						double number = 0;
						foreach (var @case in lawyer.Cases)
						{
							if (@case.CaseResultId == 1)
							{
								number++;
							}
						}
						worksheet.Cells[startLine, 5].Value = number / lawyer.Cases.Count * 100;
						startLine++;
					}
					excelPackage.SaveAs(resultFileInfo);
				}
				string fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
				string fileName = "Lawyers_Statistics.xlsx";
				return File(resultFilePath, fileType, fileName);
			}
			else
			{
				//Генерация PDF-отчета
				return null;
			}
		}

		protected override void Dispose (bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
