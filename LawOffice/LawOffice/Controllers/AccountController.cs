﻿using LawOffice.Models.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace LawOffice.Controllers
{
	public class AccountController : Controller
	{
		private UserManager<User> userManager;
		private RoleManager<Role> roleManager;
		public AccountController ()
		{
			LawOfficeIdentityDBContext db = new LawOfficeIdentityDBContext();
			UserStore<User> userStore = new UserStore<User>(db);
			userManager = new UserManager<User>(userStore);
			RoleStore<Role> roleStore = new RoleStore<Role>(db);
			roleManager = new RoleManager<Role>(roleStore);
		}
		public ActionResult Register ()
		{
			return View();
		}
		[HttpPost, ValidateAntiForgeryToken]
		public ActionResult Register (Register model)
		{
			if (ModelState.IsValid)
			{
				User user = new User
				{
					UserName = model.UserName,
					Email = model.Email,
					FullName = model.FullName,
					BirthDate = model.BirthDate
				};
				IdentityResult result = userManager.Create(user, model.Password);
				if (result.Succeeded)
				{
					userManager.AddToRole(user.Id, "User");
					return RedirectToAction("Login", "Account");
				}
				else
				{
					ModelState.AddModelError("UserName", result.Errors.First());
				}
			}
			return View(model);
		}
		[Authorize]
		public ActionResult RegisterLawyer ()
		{
			if (User.IsInRole("Administrator"))
			{
				return View();
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}
		[HttpPost, ValidateAntiForgeryToken, Authorize]
		public ActionResult RegisterLawyer (Register model)
		{
			if (User.IsInRole("Administrator"))
			{
				if (ModelState.IsValid)
				{
					User user = new User
					{
						UserName = model.UserName,
						Email = model.Email,
						FullName = model.FullName,
						BirthDate = model.BirthDate
					};
					IdentityResult result = userManager.Create(user, model.Password);
					if (result.Succeeded)
					{
						userManager.AddToRole(user.Id, "Lawyer");
						return RedirectToAction("Login", "Account");
					}
					else
					{
						ModelState.AddModelError("UserName", result.Errors.First());
					}
				}
				return View(model);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}
		public ActionResult Login (string returnUrl)
		{
			ViewBag.ReturnUrl = returnUrl;
			return View();
		}
		[HttpPost, ValidateAntiForgeryToken]
		public ActionResult Login (Login model, string returnUrl)
		{
			if (ModelState.IsValid)
			{
				User user = userManager.Find(model.UserName, model.Password);
				if (user != null)
				{
					IAuthenticationManager authenticationManager = HttpContext.GetOwinContext().Authentication;
					authenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
					ClaimsIdentity identity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
					AuthenticationProperties props = new AuthenticationProperties();
					props.IsPersistent = model.IsRememberMe;
					authenticationManager.SignIn(props, identity);
					if (Url.IsLocalUrl(returnUrl))
					{
						return Redirect(returnUrl);
					}
					else
					{
						return RedirectToAction("Index", "Home");
					}
				}
				else
				{
					ModelState.AddModelError("", "Неправильный логин или пароль");
				}
			}
			return View(model);
		}
		//[Authorize]
		//public ActionResult ChangePassword ()
		//{
		//	return View();
		//}
		//[HttpPost, Authorize, ValidateAntiForgeryToken]
		//public ActionResult ChangePassword (ChangePassword model)
		//{
		//	if (ModelState.IsValid)
		//	{
		//		User user = userManager.FindByName(HttpContext.User.Identity.Name);
		//		IdentityResult result = userManager.ChangePassword(user.Id, model.OldPassword, model.NewPassword);
		//		if (result.Succeeded)
		//		{
		//			IAuthenticationManager authenticationManager = HttpContext.GetOwinContext().Authentication;
		//			authenticationManager.SignOut();
		//			return RedirectToAction("Login", "Account");
		//		}
		//		else
		//		{
		//			ModelState.AddModelError("", " Ошибка при смене пароля");
		//		}
		//	}
		//	return View(model);
		//}
		//[Authorize]
		//public ActionResult ChangeProfile ()
		//{
		//	User user = userManager.FindByName(HttpContext.User.Identity.Name);
		//	ChangeProfile model = new ChangeProfile();
		//	model.FullName = user.FullName;
		//	model.BirthDate = user.BirthDate;
		//	return View(model);
		//}
		//[HttpPost, Authorize, ValidateAntiForgeryToken]
		//public ActionResult ChangeProfile (ChangeProfile model)
		//{
		//	if (ModelState.IsValid)
		//	{
		//		User user = userManager.FindByName(HttpContext.User.Identity.Name);
		//		user.FullName = model.FullName;
		//		user.BirthDate = model.BirthDate;
		//		IdentityResult result = userManager.Update(user);
		//		if (result.Succeeded)
		//		{
		//			ViewBag.Message = "Профиль успешно обновлен.";
		//		}
		//		else
		//		{
		//			ModelState.AddModelError("", "Ошибка при сохранении профиля.");
		//		}
		//	}
		//	return View(model);
		//}
		public ActionResult LogOut ()
		{
			IAuthenticationManager authenticationManager = HttpContext.GetOwinContext().Authentication;
			authenticationManager.SignOut();
			return RedirectToAction("Login", "Account");
		}
		// GET: Account
		public ActionResult Index ()
		{
			return View();
		}
		//[Authorize]
		//public ActionResult Profile ()
		//{
		//	return View();
		//}
	}
}