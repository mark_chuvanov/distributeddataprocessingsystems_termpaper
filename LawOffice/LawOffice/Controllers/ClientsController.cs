﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Xml;
using LawOffice.Models;
using LawOffice.Models.Security;
using LawOffice.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LawOffice.Controllers
{
	[Authorize]
	public class ClientsController : Controller
	{
		private LawOfficeDBContext db = new LawOfficeDBContext();

		// GET: Clients
		public ActionResult Index ()
		{
			if (User.IsInRole("Administrator"))
			{
				return View(db.Clients.ToList());
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		public ActionResult LoadFromAPI ()
		{
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://localhost:44334/api/Clients");
			request.Accept = "application/xml";
			WebResponse response = request.GetResponse();
			XmlDocument doc = new XmlDocument();
			doc.Load(response.GetResponseStream());
			XmlElement root = doc.DocumentElement;
			foreach (XmlElement clientNode in root)
			{
				int id = Convert.ToInt32(clientNode["Id"].InnerText);
				Client client = db.Clients.Find(id);
				if (client == null)
				{
					Client newClient = new Client()
					{
						Surname = clientNode["Surname"].InnerText,
						Name = clientNode["Name"].InnerText,
						Patronymic = clientNode["Patronymic"].InnerText,
						DateOfBirth = DateTime.Parse(clientNode["DateOfBirth"].InnerText),
						PassportSeries = int.Parse(clientNode["PassportSeries"].InnerText),
						PassportNumber = int.Parse(clientNode["PassportNumber"].InnerText),
						PlaceOfResidence = clientNode["PlaceOfResidence"].InnerText,
						Email = clientNode["Email"].InnerText
					};
					db.Clients.Add(newClient);
				}
				else
				{
					client.Surname = clientNode["Surname"].InnerText;
					client.Name = clientNode["Name"].InnerText;
					client.Patronymic = clientNode["Patronymic"].InnerText;
					client.DateOfBirth = DateTime.Parse(clientNode["DateOfBirth"].InnerText);
					client.PassportSeries = int.Parse(clientNode["PassportSeries"].InnerText);
					client.PassportNumber = int.Parse(clientNode["PassportNumber"].InnerText);
					client.PlaceOfResidence = clientNode["PlaceOfResidence"].InnerText;
				}
			}
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		// GET: Clients/Details/5
		public ActionResult Details (int? id)
		{
			if (User.IsInRole("Client"))
			{
				LawOfficeIdentityDBContext dbIdentity = new LawOfficeIdentityDBContext();
				UserStore<User> userStore = new UserStore<User>(dbIdentity);
				UserManager<User> userManager = new UserManager<User>(userStore);
				RoleStore<Role> roleStore = new RoleStore<Role>(dbIdentity);
				RoleManager<Role> roleManager = new RoleManager<Role>(roleStore);
				User user = userManager.FindByName(HttpContext.User.Identity.Name);
				try
				{
					id = db.Clients.Where(l => l.Email == user.Email).FirstOrDefault().Id;
				}
				catch { }
				if (id == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				ClientDetails clientDetails = new ClientDetails();
				clientDetails.Client = db.Clients.Find(id);
				clientDetails.Cases = db.Cases.Include(s => s.CaseStatus).Include(r => r.CaseResult).Where(c => c.ClientId == id).ToList();
				if (clientDetails.Client == null)
				{
					return HttpNotFound();
				}
				return View(clientDetails);
			}
			else
			{
				if (User.IsInRole("Administrator"))
				{
					if (id == null)
					{
						return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
					}
					ClientDetails clientDetails = new ClientDetails();
					clientDetails.Client = db.Clients.Find(id);
					clientDetails.Cases = db.Cases.Include(s => s.CaseStatus).Include(r => r.CaseResult).Where(c => c.ClientId == id).ToList();
					if (clientDetails.Client == null)
					{
						return HttpNotFound();
					}
					return View(clientDetails);
				}
				else
				{
					return new HttpStatusCodeResult(403);
				}
			}
		}

		// GET: Clients/Create
		public ActionResult Create ()
		{
			if (User.IsInRole("Administrator"))
			{
				return View();
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// POST: Clients/Create
		// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
		// сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create ([Bind(Include = "Id,Surname,Name,Patronymic,DateOfBirth,PassportSeries,PassportNumber,PlaceOfResidence,Photo,Email")] Client client)
		{
			if (User.IsInRole("Administrator"))
			{
				if (client.DateOfBirth >= DateTime.Now)
				{
					ModelState.AddModelError("DateOfBirth", "Введена некорректная дата рождения");
				}
				if (ModelState.IsValid)
				{
					db.Clients.Add(client);
					db.SaveChanges();
					return RedirectToAction("Index");
				}
				return View(client);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// GET: Clients/Edit/5
		public ActionResult Edit (int? id)
		{
			if (User.IsInRole("Administrator"))
			{
				if (id == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				Client client = db.Clients.Find(id);
				if (client == null)
				{
					return HttpNotFound();
				}
				return View(client);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// POST: Clients/Edit/5
		// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
		// сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit ([Bind(Include = "Id,Surname,Name,Patronymic,DateOfBirth,PassportSeries,PassportNumber,PlaceOfResidence,Photo,Email")] Client client)
		{
			if (User.IsInRole("Administrator"))
			{
				if (ModelState.IsValid)
				{
					db.Entry(client).State = EntityState.Modified;
					db.SaveChanges();
					return RedirectToAction("Index");
				}
				return View(client);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// GET: Clients/Delete/5
		//public ActionResult Delete (int? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	Client client = db.Clients.Find(id);
		//	if (client == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(client);
		//}

		// POST: Clients/Delete/5
		//[HttpPost, ActionName("Delete")]
		//[ValidateAntiForgeryToken]
		//public ActionResult DeleteConfirmed (int id)
		//{
		//	Client client = db.Clients.Find(id);
		//	db.Clients.Remove(client);
		//	db.SaveChanges();
		//	return RedirectToAction("Index");
		//}

		protected override void Dispose (bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}