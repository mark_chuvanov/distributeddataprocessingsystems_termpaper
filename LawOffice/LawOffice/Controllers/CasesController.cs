﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Windows.Media;
using System.Xml;
using LawOffice.Models;
using LawOffice.Synchronisation;
using LawOffice.ViewModels;
using MailKit.Net.Smtp;
using Microsoft.Ajax.Utilities;
using MimeKit;
using OfficeOpenXml;

namespace LawOffice.Controllers
{
	[Authorize]
	public class CasesController : Controller
	{
		private LawOfficeDBContext db = new LawOfficeDBContext();

		// GET: Cases
		public ActionResult Index ()
		{
			if (User.IsInRole("Administrator"))
			{
				var cases = db.Cases.Include(C => C.CaseResult).Include(S => S.CaseStatus).Include(C => C.Client);
				return View(cases.ToList());
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		public ActionResult LoadFromAPI ()
		{
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://localhost:44334/api/Cases");
			request.Accept = "application/xml";
			WebResponse response = request.GetResponse();
			XmlDocument doc = new XmlDocument();
			doc.Load(response.GetResponseStream());
			XmlElement root = doc.DocumentElement;
			foreach (XmlElement caseNode in root)
			{
				int id = Convert.ToInt32(caseNode["Id"].InnerText);
				Case @case = db.Cases.Find(id);
				if (@case == null)
				{
					Case newCase = new Case()
					{
						Number = caseNode["Number"].InnerText,
						ClientId = int.Parse(caseNode["ClientId"].InnerText),
						OpeningDate = DateTime.Parse(caseNode["OpeningDate"].InnerText),
						ClosingDate = DateTime.Parse(caseNode["ClosingDate"].InnerText),
						CaseStatusId = int.Parse(caseNode["CaseStatusId"].InnerText),
						CaseResultId = int.Parse(caseNode["CaseResultId"].InnerText),
						Costs = int.Parse(caseNode["Costs"].InnerText),
					};
					db.Cases.Add(newCase);
				}
				else
				{
					@case.Number = caseNode["Number"].InnerText;
					@case.ClientId = int.Parse(caseNode["ClientId"].InnerText);
					@case.OpeningDate = DateTime.Parse(caseNode["OpeningDate"].InnerText);
					@case.ClosingDate = DateTime.Parse(caseNode["ClosingDate"].InnerText);
					@case.CaseStatusId = int.Parse(caseNode["CaseStatusId"].InnerText);
					@case.CaseResultId = int.Parse(caseNode["CaseResultId"].InnerText);
					@case.Costs = int.Parse(caseNode["Costs"].InnerText);
				}
			}
			SynchronisationFromAPI synchronisation = new SynchronisationFromAPI();
			synchronisation.LoadCaseLegalProceduresFromAPI();
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		// GET: Cases/Details/5
		public ActionResult Details (int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			CaseDetails caseDetails = new CaseDetails();
			caseDetails.Case = db.Cases.Include(c => c.Client).Include(l => l.Lawyers).Include(c => c.CaseResult).Include(c => c.CaseStatus).Where(c => c.Id == id).FirstOrDefault();
			caseDetails.Lawyers = caseDetails.Case.Lawyers;
			caseDetails.CaseLegalProcedures = db.CasesLegalProcedures.Where(p => p.CaseId == id).ToList();
			if (caseDetails.Case == null)
			{
				return HttpNotFound();
			}
			return View(caseDetails);
		}

		// GET: Cases/Create
		public ActionResult Create (int id)
		{
			if (User.IsInRole("Administrator"))
			{
				ViewBag.CaseResultId = new SelectList(db.CaseResults, "Id", "Name");
				ViewBag.CaseStatusId = new SelectList(db.CaseStatuses, "Id", "Name");
				ViewBag.ClientId = new SelectList(db.Clients, "Id", "Surname");
				ViewBag.LawyerId = new SelectList(db.Lawyers, "Id", "FIO");
				ViewData["Id"] = id;
				return View();
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// POST: Cases/Create
		// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
		// сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create ([Bind(Include = "Id,Number,ClientId,OpeningDate,ClosingDate,CaseStatusId,CaseResultId,Costs")] Case @case, int LawyerId)
		{
			if (User.IsInRole("Administrator"))
			{

				@case.CaseStatusId = 1;
				@case.CaseResultId = 4;
				@case.OpeningDate = DateTime.Now;
				@case.ClosingDate = DateTime.Now;
				bool isContains = false;
				foreach (Case c in db.Cases)
				{
					if (c.Number == @case.Number)
					{
						isContains = true;
					}
				}
				if (isContains)
				{
					ModelState.AddModelError("Number", "Дело должен иметь уникальный номер");
				}
				if (ModelState.IsValid)
				{
					@case.Lawyers.Add(db.Lawyers.Where(l => l.Id == LawyerId).First());
					db.Cases.Add(@case);
					db.SaveChanges();
					return RedirectToAction("Details", "Clients", new { id = @case.ClientId });
				}

				ViewBag.CaseResultId = new SelectList(db.CaseResults, "Id", "Name", @case.CaseResultId);
				ViewBag.CaseStatusId = new SelectList(db.CaseStatuses, "Id", "Name", @case.CaseStatusId);
				ViewBag.ClientId = new SelectList(db.Clients, "Id", "Surname", @case.ClientId);
				ViewBag.LawyerId = new SelectList(db.Lawyers, "Id", "Surname");
				return View(@case);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// GET: Cases/Edit/5
		public ActionResult Edit (int? id)
		{
			if (User.IsInRole("Administrator") || User.IsInRole("Lawyer"))
			{
				if (id == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				Case @case = db.Cases.Find(id);
				if (@case == null)
				{
					return HttpNotFound();
				}
				if (@case.CaseStatusId == 2)
				{
					return RedirectToAction("Index");
				}
				ViewBag.CaseResultId = new SelectList(db.CaseResults, "Id", "Name", @case.CaseResultId);
				ViewBag.CaseStatusId = new SelectList(db.CaseStatuses, "Id", "Name", @case.CaseStatusId);
				ViewBag.ClientId = new SelectList(db.Clients, "Id", "Surname", @case.ClientId);
				return View(@case);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}
		// POST: Cases/Edit/5
		// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
		// сведения см. в разделе https://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit ([Bind(Include = "Id,Number,ClientId,OpeningDate,ClosingDate,CaseStatusId,CaseResultId,Costs")] Case @case)
		{
			if (User.IsInRole("Administrator") || User.IsInRole("Lawyer"))
			{
				if (@case.CaseResultId != 4)
				{
					@case.CaseStatusId = 2;
					@case.Costs = GetCaseCosts(@case.Id, @case.CaseResultId);
					@case.ClosingDate = DateTime.Now;
				}
				if (ModelState.IsValid)
				{
					db.Entry(@case).State = EntityState.Modified;
					db.SaveChanges();
					@case = db.Cases.Include(c => c.Client).Where(c => c.Id == @case.Id).First();
					SendEmail(@case);
					return RedirectToAction("Index");
				}
				ViewBag.CaseResultId = new SelectList(db.CaseResults, "Id", "Name", @case.CaseResultId);
				ViewBag.CaseStatusId = new SelectList(db.CaseStatuses, "Id", "Name", @case.CaseStatusId);
				ViewBag.ClientId = new SelectList(db.Clients, "Id", "Surname", @case.ClientId);
				return View(@case);
			}
			else
			{
				return new HttpStatusCodeResult(403);
			}
		}

		// GET: Cases/Delete/5
		//public ActionResult Delete (int? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	Case @case = db.Cases.Find(id);
		//	if (@case == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(@case);
		//}

		// POST: Cases/Delete/5
		//[HttpPost, ActionName("Delete")]
		//[ValidateAntiForgeryToken]
		//public ActionResult DeleteConfirmed (int id)
		//{
		//	Case @case = db.Cases.Find(id);
		//	db.Cases.Remove(@case);
		//	db.SaveChanges();
		//	return RedirectToAction("Index");
		//}

		[Authorize(Roles = "Administrator")]
		public FileResult GetEarnings (int id)
		{
			if (id == 1)
			{
				ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
				string originalFilePath = Server.MapPath("~/Content/Reports/earnings.xlsx");
				FileInfo originalFileInfo = new FileInfo(originalFilePath);
				string resultFilePath = Server.MapPath("~/Content/Reports/report.xlsx");
				FileInfo resultFileInfo = new FileInfo(resultFilePath);
				using (ExcelPackage excelPackage = new ExcelPackage(originalFileInfo))
				{
					excelPackage.Workbook.Properties.Author = "Mark Chuvanov";
					excelPackage.Workbook.Properties.Title = "Доход";
					excelPackage.Workbook.Properties.Subject = "Заработок";
					excelPackage.Workbook.Properties.Created = DateTime.Now;
					ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets["Earnings"];
					int startLine = 2;
					var cases = db.Cases.ToList();
					int startYear = 2019;
					int endYear = DateTime.Now.Year;
					for (int i = startYear; i <= endYear; i++)
					{
						worksheet.Cells[startLine, 1].Value = i;
						int earnings = 0;
						foreach (var @case in cases)
						{
							if (@case.ClosingDate.Year == i)
							{
								earnings += @case.Costs;
							}
						}
						worksheet.Cells[startLine, 2].Value = earnings;
						startLine++;
					}
					excelPackage.SaveAs(resultFileInfo);
				}
				string fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
				string fileName = "Earnings.xlsx";
				return File(resultFilePath, fileType, fileName);
			}
			else
			{
				//Генерация PDF-отчета
				return null;
			}
		}

		[Authorize(Roles = "Administrator")]
		public FileResult GetCases (int id, DateTime startDate, DateTime endDate)
		{
			if (id == 1)
			{
				ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
				string originalFilePath = Server.MapPath("~/Content/Reports/cases.xlsx");
				FileInfo originalFileInfo = new FileInfo(originalFilePath);
				string resultFilePath = Server.MapPath("~/Content/Reports/report.xlsx");
				FileInfo resultFileInfo = new FileInfo(resultFilePath);
				using (ExcelPackage excelPackage = new ExcelPackage(originalFileInfo))
				{
					excelPackage.Workbook.Properties.Author = "Mark Chuvanov";
					excelPackage.Workbook.Properties.Title = "Список дел за выбранный период";
					excelPackage.Workbook.Properties.Subject = "Дела";
					excelPackage.Workbook.Properties.Created = DateTime.Now;
					ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets["Cases"];
					int startLine = 2;
					var cases = db.Cases.Include(c => c.Client).Include(c => c.CaseStatus).Include(c => c.CaseResult).ToList();
					for (int i = 0; i < cases.Count; i++)
					{
						if (cases[i].CaseStatusId == 2 & cases[i].OpeningDate > startDate & cases[i].ClosingDate < endDate)
						{
							worksheet.Cells[startLine, 1].Value = cases[i].Number;
							worksheet.Cells[startLine, 2].Value = cases[i].Client.Surname + " " + cases[i].Client.Name + " " + cases[i].Client.Patronymic;
							worksheet.Cells[startLine, 3].Value = cases[i].OpeningDate.ToString("dd.MM.yyyy");
							worksheet.Cells[startLine, 4].Value = cases[i].ClosingDate.ToString("dd.MM.yyyy");
							worksheet.Cells[startLine, 5].Value = cases[i].CaseStatus.Name;
							worksheet.Cells[startLine, 6].Value = cases[i].CaseResult.Name;
							startLine++;
						}
					}
					excelPackage.SaveAs(resultFileInfo);
				}
				string fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
				string fileName = "Cases.xlsx";
				return File(resultFilePath, fileType, fileName);
			}
			else
				return null;
		}

		protected override void Dispose (bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		[NonAction]
		private int GetCaseCosts (int id, int resultId)
		{
			int costs = 0;
			var caseLegalProcedures = db.CasesLegalProcedures.Include(c => c.LegalProcedure).Where(c => c.CaseId == id).ToList();
			switch (resultId)
			{
				case 1:
				{
					foreach (var procedure in caseLegalProcedures)
					{
						costs += procedure.LegalProcedure.Costs;
					}
					costs += 30000;
					break;
				}
				case 2:
				{
					foreach (var procedure in caseLegalProcedures)
					{
						costs += procedure.LegalProcedure.Costs;
					}
					break;
				}
				case 3:
				{
					costs = 5000;
					break;
				}
			}
			return costs;
		}

		[NonAction]
		private void SendEmail (Case @case)
		{
			var emailMessage = new MimeMessage();
			emailMessage.From.Add(new MailboxAddress("Администрация адвокатской конторы", "markchuvanov@mail.ru"));
			emailMessage.To.Add(new MailboxAddress("", @case.Client.Email));
			emailMessage.Subject = "Закрытие дела № " + @case.Number;
			emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
			{
				Text = "Уважаемый " + @case.Client.Name + " " + @case.Client.Patronymic + "!" + " Сообщаем Вам, что дело № " + @case.Number + " закрыто. За ведение дела Вам необходимо заплатить вознаграждение в размере " + @case.Costs + " рублей."
			};
			using (var smtpClient = new SmtpClient())
			{
				smtpClient.Connect("smtp.mail.ru", 587, false);
				smtpClient.Authenticate("markchuvanov@mail.ru", "Zpyf.?xnjybxtujytpyf./");
				smtpClient.Send(emailMessage);
				smtpClient.Disconnect(true);
			}
		}
	}
}